import 'package:firebase_auth/firebase_auth.dart';
import 'package:flowing_project/screens/home_page/home_page_new.dart';
import 'package:flowing_project/screens/login/login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthService {
  handleAuth() {
    return StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return HomePageNew();
          } else {
            return LoginPage();
          }
        });
  }

  signOut() {
    FirebaseAuth.instance.signOut();
  }

  signIn(email, password) {
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password)
        .then((user) {
      print('Signed in');
    }).catchError((e) {
      Get.defaultDialog(
        title: 'Errore Login',
        middleText: 'You cannot enter with this credential',
        textCancel: 'go back',
        confirmTextColor: Colors.white,
        onCancel: () => Get.back(),
      );
      print(e);
    });
  }

  Future createNewUser(String email, String password) async {
    try {
      UserCredential result = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      User user = result.user;
      return user;
    } catch (e) {
      Get.defaultDialog(
        title: 'Error Login',
      );
      print('eeeee');
      print(e.toString());
    }
  }
}
