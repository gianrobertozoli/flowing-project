import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

class NetworkManager {
  Future<http.Response> get(url, {Map<String, String> parameters}) async {
    String finalURL = '';

    if (parameters != null && parameters.isNotEmpty) {
      finalURL = url + "?" + Uri(queryParameters: parameters).query;
    } else {
      finalURL = url;
    }

    final response = http.get(Uri.parse(finalURL),
        headers: {HttpHeaders.contentLanguageHeader: 'IT-it'});

    return response;
  }

  Future<http.Response> delete(url, {Map<String, String> parameters}) async {
    String finalURL = '';

    if (parameters != null && parameters.isNotEmpty) {
      finalURL = url + "?" + Uri(queryParameters: parameters).query;
    } else {
      finalURL = url;
    }

    final response = http.get(Uri.parse(finalURL),
        headers: {HttpHeaders.contentLanguageHeader: 'IT-it'});

    return response;
  }

  Future<http.Response> post(url, {Map<String, dynamic> parameters}) async {
    final response = http.post(Uri.parse(url),
        body: (parameters != null && parameters.isNotEmpty)
            ? jsonEncode(parameters)
            : "",
        headers: {
          HttpHeaders.contentLanguageHeader: 'IT-it',
          HttpHeaders.contentTypeHeader: 'application/json'
        });

    return response;
  }

  Future<http.Response> patch(url,
      {Map<String, dynamic> parameters = null}) async {
    final response = http.patch(Uri.parse(url),
        body: (parameters != null && parameters.isNotEmpty)
            ? jsonEncode(parameters)
            : "",
        headers: {HttpHeaders.contentLanguageHeader: 'IT-it'});

    return response;
  }
}
