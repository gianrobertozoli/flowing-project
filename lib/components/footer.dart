import 'package:ant_icons/ant_icons.dart';
import 'package:flowing_project/configuration/colors_style.dart';
import 'package:flowing_project/configuration/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth > 1400) {
        return FooterLarge();
      } else if (constraints.maxWidth > 800 && constraints.maxWidth < 1400) {
        return FooterMedium();
      } else if (constraints.maxWidth > 600 && constraints.maxWidth < 800) {
        return FooterSmall();
      } else {
        return FooterExtraSmall();
      }
    });
  }
}

class FooterLarge extends StatelessWidget {
  const FooterLarge({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,
      color: BrandColors.colorFooter,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 64),
            child: Row(children: [
              Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Company', style: footerColumnTitleStyle),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          'Home',
                          style: footerColumnItemStyle,
                        ),
                        Text(
                          'About',
                          style: footerColumnItemStyle,
                        ),
                        Text(
                          'Crew',
                          style: footerColumnItemStyle,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Privacy and Legals',
                            style: footerColumnTitleStyle),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          'Legals',
                          style: footerColumnItemStyle,
                        ),
                        Text(
                          'Privacy Policy',
                          style: footerColumnItemStyle,
                        ),
                        Text(
                          'Cookie Policy',
                          style: footerColumnItemStyle,
                        ),
                        Text(
                          'Privacy Policy Pica',
                          style: footerColumnItemStyle,
                        ),
                        Text(
                          'Terms and conditions',
                          style: footerColumnItemStyle,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Social', style: footerColumnTitleStyle),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            Icon(
                              AntIcons.facebook,
                              color: BrandColors.colorFooterColumnItem,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              'facebook',
                              style: footerColumnItemStyle,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            Icon(
                              AntIcons.instagram_outline,
                              color: BrandColors.colorFooterColumnItem,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Instagram',
                              style: footerColumnItemStyle,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            Icon(
                              AntIcons.linkedin,
                              color: BrandColors.colorFooterColumnItem,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              'LinkedIn',
                              style: footerColumnItemStyle,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/icons/app_Store.svg',
                      height: 48,
                    ),
                    SvgPicture.asset(
                      'assets/icons/google-play.svg',
                      height: 48,
                    ),
                    Image.asset(
                      'assets/icons/huawei.png',
                      height: 70,
                    )
                  ],
                ),
              ),
            ]),
          ),
          Divider(
            height: 2,
            color: BrandColors.colorFooterColumnItem,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '2019 – 2021 Rocket Group S.p.A.',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'VAT no. 0123456789',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Fully-paid share capital €1.000.000',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Patent-protected technology',
                  style: footerColumnItemStyle,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class FooterMedium extends StatelessWidget {
  const FooterMedium({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 400,
      color: BrandColors.colorFooter,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 64),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Company', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text('Home', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'About',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Crew',
                        style: footerColumnItemStyle,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Privacy and Legals', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Legals',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Privacy Policy',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Cookie Policy',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Privacy Policy Pica',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Terms and conditions',
                        style: footerColumnItemStyle,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Social', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/facebook.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'facebook',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/instagram.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Instagram',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/linkedin.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'LinkedIn',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/icons/app_Store.svg',
                    height: 48,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  SvgPicture.asset(
                    'assets/icons/google-play.svg',
                    height: 48,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/icons/huawei.png',
                    height: 70,
                  )
                ],
              ),
            ]),
          ),
          Divider(
            height: 2,
            color: BrandColors.colorFooterColumnItem,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '2019 – 2021 Rocket Group S.p.A.',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'VAT no. 0123456789',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Fully-paid share capital €1.000.000',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Patent-protected technology',
                  style: footerColumnItemStyle,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class FooterSmall extends StatelessWidget {
  const FooterSmall({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 500,
      color: BrandColors.colorFooter,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 64),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Company', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text('Home', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'About',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Crew',
                        style: footerColumnItemStyle,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Privacy and Legals', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Legals',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Privacy Policy',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Cookie Policy',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Privacy Policy Pica',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Terms and conditions',
                        style: footerColumnItemStyle,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Social', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/facebook.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'facebook',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/instagram.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Instagram',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/linkedin.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'LinkedIn',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/icons/app_Store.svg',
                    height: 48,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  SvgPicture.asset(
                    'assets/icons/google-play.svg',
                    height: 48,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/icons/huawei.png',
                    height: 70,
                  )
                ],
              ),
            ]),
          ),
          Divider(
            height: 2,
            color: BrandColors.colorFooterColumnItem,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '2019 – 2021 Rocke Group S.p.A.',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'VAT no. 0123456789',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Fully-paid share capital €1.000.000',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Patent-protected technology',
                  style: footerColumnItemStyle,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class FooterExtraSmall extends StatelessWidget {
  const FooterExtraSmall({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 800,
      color: BrandColors.colorFooter,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 64),
            child: Column(children: [
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Company', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Home',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'About',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Crew',
                        style: footerColumnItemStyle,
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Privacy and Legals', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Legals',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Privacy Policy',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Cookie Policy',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Privacy Policy Pica',
                        style: footerColumnItemStyle,
                      ),
                      Text(
                        'Terms and conditions',
                        style: footerColumnItemStyle,
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Social', style: footerColumnTitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/facebook.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'facebook',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/instagram.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Instagram',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/linkedin.svg',
                            height: 25,
                            color: BrandColors.colorFooterColumnItem,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'LinkedIn',
                            style: footerColumnItemStyle,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/icons/app_Store.svg',
                    height: 48,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  SvgPicture.asset(
                    'assets/icons/google-play.svg',
                    height: 48,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/icons/huawei.png',
                    height: 70,
                  )
                ],
              ),
            ]),
          ),
          Divider(
            height: 2,
            color: BrandColors.colorFooterColumnItem,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '2019 – 2021 Rocket Group S.p.A.',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'VAT no. 0123456789',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Fully-paid share capital €1.000.000',
                  style: footerColumnItemStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Patent-protected technology',
                  style: footerColumnItemStyle,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
