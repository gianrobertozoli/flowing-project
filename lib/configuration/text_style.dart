import 'package:flowing_project/configuration/colors_style.dart';
import 'package:flutter/material.dart';

final TextStyle surtitleStyle = TextStyle(
    color: BrandColors.colorTextPrimary,
    fontWeight: FontWeight.bold,
    fontSize: 32);
final TextStyle titleStyle = TextStyle(
    color: BrandColors.colorTextPrimary,
    fontWeight: FontWeight.bold,
    fontSize: 62);
final TextStyle buttonStyle = TextStyle(
    color: BrandColors.colorTextSecondary,
    fontWeight: FontWeight.w400,
    fontSize: 20);
final TextStyle footerColumnTitleStyle = TextStyle(
    color: BrandColors.colorFooterColumnTitle,
    fontWeight: FontWeight.w400,
    fontSize: 16);
final TextStyle footerColumnItemStyle = TextStyle(
    color: BrandColors.colorFooterColumnItem,
    fontWeight: FontWeight.w400,
    fontSize: 14);
