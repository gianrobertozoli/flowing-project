import 'package:flutter/material.dart';

class BrandColors {
  static const Color colorTextPrimary = Colors.black;
  static const Color colorTextSecondary = Colors.white;

  static const Color colorFooter = Color(0XFF00001A);
  static const Color colorFooterColumnTitle = Color(0XFFe9ecef);
  static const Color colorFooterColumnItem = Color(0XFF6c757d);

  static const Color kPrimaryColor = Color(0xFFFF3B1D);
  static const Color kDarkBlackColor = Color(0xFF191919);
  static const Color kBgColor = Color(0xFFE7E7E7);
  static const Color kBodyTextColor = Color(0xFF666666);
}
