import 'environment.dart';

class URLs {
  static String baseURL = Environment.isProduction
      ? "https://production-server.com/api"
      : "https://api.spacexdata.com/v4";

  static String rockets = "$baseURL/rockets/";
  static String crew = "$baseURL/crew/";
}
