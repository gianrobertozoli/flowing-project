import 'package:flowing_project/components/footer.dart';
import 'package:flowing_project/configuration/responsive_helper.dart';
import 'package:flowing_project/screens/home_page/component/header.dart';
import 'package:flowing_project/screens/home_page/component/rockets.dart';
import 'package:flowing_project/screens/home_page/component/side_menu.dart';
import 'package:flowing_project/screens/home_page/controller/menu_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePageNew extends StatelessWidget {
  MenuController _controller = Get.put(MenuController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _controller.scaffoldkey,
      drawer: SideMenu(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Header(),
            Container(
              padding: EdgeInsets.all(kDefaultPadding),
              constraints: BoxConstraints(maxWidth: kMaxWidth),
              child: SafeArea(
                  child: Column(
                children: [
                  Rockets(),
                ],
              )),
            ),
            Footer()
          ],
        ),
      ),
    );
  }
}
