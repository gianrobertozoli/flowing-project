import 'dart:html';

import 'package:ant_icons/ant_icons.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flowing_project/configuration/responsive_helper.dart';
import 'package:flowing_project/screens/home_page/controller/home_page_controller.dart';
import 'package:flowing_project/screens/home_page/model/rockets_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class Rockets extends StatefulWidget {
  @override
  _RocketsState createState() => _RocketsState();
}

class _RocketsState extends State<Rockets> {
  HomePageController controller = Get.put(HomePageController());
  final userUid = FirebaseAuth.instance.currentUser.uid;

  @override
  Widget build(BuildContext context) {
    Stream documentStream =
        FirebaseFirestore.instance.collection('users').doc(userUid).snapshots();
    final mediaQuery = MediaQuery.of(context);

    return Obx(() {
      return controller.isLoading.value
          ? Container(
              child: Center(
                child: Text('Loading...'),
              ),
            )
          : ListView(
              shrinkWrap: true,
              children: [_buildArticlesGrid(mediaQuery, documentStream)],
            );
    });
  }

  _buildArticlesGrid(
      MediaQueryData mediaQuery, Stream<dynamic> documentStream) {
    List<GridTile> tiles = [];
    controller.rockets.forEach((rockets) {
      tiles.add(_buildArticleTile(rockets, mediaQuery, documentStream));
    });
    return Padding(
      padding: responsivePadding(mediaQuery),
      child: GridView.count(
        crossAxisCount: responsiveNumGridTiles(mediaQuery),
        mainAxisSpacing: 30.0,
        crossAxisSpacing: 30.0,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: tiles,
      ),
    );
  }

  _buildArticleTile(RocketsModel rockets, MediaQueryData mediaQuery,
      Stream<dynamic> documentStream) {
    return GridTile(
        child: GestureDetector(
      onTap: () => {},
      child: FlipCard(
          direction: FlipDirection.HORIZONTAL,
          front: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: responsiveImageHeight(mediaQuery),
                  alignment: Alignment.bottomCenter,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    ),
                    image: DecorationImage(
                      image: NetworkImage(rockets.flickrImages[0]),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                alignment: Alignment.center,
                height: responsiveTitleHeight(mediaQuery),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0, 1),
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: Text(
                  rockets.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                  maxLines: 2,
                ),
              ),
            ],
          ),
          back: Container(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Name: ${rockets.name}',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Text('Name: ${rockets.description}'),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Container(
                      width: 10,
                      height: 10,
                      decoration: BoxDecoration(
                          color: rockets.active ? Colors.green : Colors.grey,
                          shape: BoxShape.circle),
                    ),
                    Text(rockets.active ? 'Active' : 'Inactive')
                  ],
                ),
                StreamBuilder(
                    stream: documentStream,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return Text('Something went wrong');
                      }

                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Text("Loading");
                      }
                      if (snapshot.data['favorites_rockets'] != null) {
                        if (snapshot.data['favorites_rockets']
                            .contains(rockets.id)) {
                          return IconButton(
                              icon: Icon(AntIcons.heart),
                              onPressed: () {
                                final retFav =
                                    snapshot.data['favorites_rockets'];
                                retFav.remove(rockets.id);
                                controller.updateUser(retFav);
                              });
                        } else {
                          return IconButton(
                              icon: Icon(AntIcons.heart_outline),
                              onPressed: () {
                                final retFav =
                                    snapshot.data['favorites_rockets'];

                                retFav.add(rockets.id);

                                controller.updateUser(retFav);
                              });
                        }
                      } else {
                        return IconButton(
                            icon: Icon(AntIcons.heart_outline),
                            onPressed: () {
                              print('is null');
                              List<String> retFav = [];
                              retFav.add(rockets.id);
                              controller.updateUser(retFav);
                            });
                      }
                    }),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                        onTap: () => _launchURL(rockets.wikipedia),
                        child: Text('${rockets.wikipedia}'))
                  ],
                )
              ],
            ),
          )),
    ));
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
