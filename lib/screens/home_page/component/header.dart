import 'package:flowing_project/configuration/colors_style.dart';
import 'package:flowing_project/configuration/responsive.dart';
import 'package:flowing_project/configuration/responsive_helper.dart';
import 'package:flowing_project/screens/home_page/controller/menu_controller.dart';
import 'package:flowing_project/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'web_menu.dart';

class Header extends StatelessWidget {
  final MenuController _controller = Get.put(MenuController());

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: BrandColors.kDarkBlackColor,
      child: SafeArea(
        child: Column(
          children: [
            Container(
              constraints: BoxConstraints(maxWidth: kMaxWidth),
              padding: EdgeInsets.all(kDefaultPadding),
              child: Column(
                children: [
                  Row(
                    children: [
                      if (!Responsive.isDesktop(context))
                        IconButton(
                          icon: Icon(
                            Icons.menu,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            _controller.openOrCloseDrawer();
                          },
                        ),
                      Text(
                        'Rockets App',
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Color(0xffffffff),
                        ),
                      ),
                      Spacer(),
                      if (Responsive.isDesktop(context)) WebMenu(),
                      Spacer(),
                      RaisedButton(
                        onPressed: () {
                          AuthService().signOut();
                        },
                        child: Center(
                          child: Text('Sign out'),
                        ),
                        color: Colors.red,
                      )
                      // Socal
                    ],
                  ),
                  SizedBox(height: kDefaultPadding * 2),
                  Text(
                    "SpaceX REST API.",
                    style: TextStyle(
                      fontSize: 32,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: kDefaultPadding),
                    child: Text(
                      "Open Source REST API for rocket",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Raleway',
                        height: 1.5,
                      ),
                    ),
                  ),
                  // FittedBox(child: Image.asset('assets/images/rocket.png')),
                  if (Responsive.isDesktop(context))
                    SizedBox(height: kDefaultPadding),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
