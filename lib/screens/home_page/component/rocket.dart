import 'package:ant_icons/ant_icons.dart';
import 'package:flowing_project/screens/home_page/controller/rocket_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Rocket extends StatelessWidget {
  RocketController rocketController = Get.put(RocketController());

  bool get _hasAlredyFlown =>
      rocketController.rocket.value.firstFlight.isBefore(DateTime.now());
  int get _daysSinceFirstFlight => rocketController.rocket.value.firstFlight
      .difference(DateTime.now())
      .abs()
      .inDays;

  String get _firstFlightLabel {
    final date = rocketController.rocket.value.firstFlight;
    return '${date.year}-${date.month}-${date.day}';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 100,
            width: 200,
            child: Image(
              image:
                  NetworkImage(rocketController.rocket.value.flickrImages[0]),
              fit: BoxFit.cover,
            )),
        ListTile(
          title: Text(
            rocketController.rocket.value.name,
          ),
          subtitle:
              rocketController.rocket.value.active ? null : Text('Inactive'),
          leading: IconButton(
            onPressed: () {},
            icon: Icon(AntIcons.heart),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(rocketController.rocket.value.description),
        ),
        ListTile(
          leading: Icon(AntIcons.colum_height),
          title: Text('${rocketController.rocket.value.height.meters}'),
          subtitle: Text('in diameter'),
        ),
        Divider(),
        ListTile(
          leading: Icon(AntIcons.column_width),
          title: Text('${rocketController.rocket.value.diameter.meters}'),
          subtitle: Text('in height'),
        ),
        Divider(),
        ListTile(
          leading: Icon(AntIcons.arrow_down),
          title: Text('${rocketController.rocket.value.mass.kg}'),
          subtitle: Text('total mass'),
        ),
        Divider(),
        ListTile(
          leading: Icon(AntIcons.calendar),
          title: Text(_hasAlredyFlown
              ? '$_daysSinceFirstFlight days since first flight'
              : '$_daysSinceFirstFlight days until first flight'),
          subtitle: Text(_hasAlredyFlown
              ? 'First flew on $_firstFlightLabel'
              : 'Scheduled to fly on $_firstFlightLabel'),
        ),
        // Padding(
        //   padding: const EdgeInsets.all(16.0),
        //   child: SizedBox(
        //       height: 56,
        //       child: ElevatedButton(
        //           onPressed: () {
        //             launch(rocketController.rocket.value.wikipedia);
        //           },
        //           child: Text('Open Wikipedia Article'))),
        // )
      ],
    );
  }
}
