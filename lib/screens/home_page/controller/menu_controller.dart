import 'package:flowing_project/screens/crew/crew_screen.dart';
import 'package:flowing_project/screens/home_page/home_page_new.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MenuController extends GetxController {
  RxInt _selectedIndex = 0.obs;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int get selectedIndex => _selectedIndex.value;
  List<String> get menuItems => ["Home", "Crew", "About"];
  GlobalKey<ScaffoldState> get scaffoldkey => _scaffoldKey;

  void openOrCloseDrawer() {
    if (_scaffoldKey.currentState.isDrawerOpen) {
      _scaffoldKey.currentState.openEndDrawer();
    } else {
      _scaffoldKey.currentState.openDrawer();
    }
  }

  void setMenuIndex(int index) {
    _selectedIndex.value = index;
    if (index == 1) {
      Get.to(() => CrewScreen());
    } else if (index == 0) {
      Get.to(() => HomePageNew());
    } else {
      Get.to(() => {});
    }
  }
}
