import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flowing_project/screens/home_page/model/rockets_model.dart';
import 'package:flowing_project/screens/home_page/service/home_page_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePageController extends GetxController {
  var isLoading = true.obs;
  final rockets = List<RocketsModel>().obs;
  RxBool detail = false.obs;

  @override
  void onInit() {
    super.onInit();
    getRockets();
  }

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  Future<void> updateUser(List<dynamic> rocketId) {
    final userUid = FirebaseAuth.instance.currentUser.uid;
    return users
        .doc(userUid)
        .update({'favorites_rockets': rocketId})
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  //Fetch Rockets
  void getRockets() {
    try {
      isLoading(true);
      HomePageService.getRockets().then((resp) {
        isLoading(false);
        rockets.value = resp;
        print('Rockets $rockets');
      }, onError: (err) {
        isLoading(false);
        showSnackBar("Error", err.toString(), Colors.red);
      });
    } catch (exception) {
      isLoading(false);
      print('exc ${exception.toString()}');
      showSnackBar("Exception 1", exception.toString(), Colors.red);
    }
  }

  // snack bar
  showSnackBar(String title, String message, Color backgroundColor) {
    Get.snackbar(title, message,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: backgroundColor,
        colorText: Colors.white);
  }
}
