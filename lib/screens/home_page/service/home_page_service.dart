import 'package:flowing_project/configuration/urls.dart';
import 'package:flowing_project/screens/home_page/model/rockets_model.dart';
import 'package:flowing_project/services/network_manager.dart';

class HomePageService {
  static NetworkManager manager = NetworkManager();

  static Future<List<RocketsModel>> getRockets() async {
    try {
      final response = await manager.get(URLs.rockets);
      if (response != null) {
        print("STATUS CODE: ${response.statusCode}");

        if (response.statusCode >= 200 && response.statusCode <= 299) {
          return rocketsModelFromJson(response.body);
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (exeption) {
      return Future.error(exeption.toString());
    }
  }
}
