// To parse this JSON data, do
//
//     final crewModel = crewModelFromJson(jsonString);

import 'dart:convert';

List<CrewModel> crewModelFromJson(String str) =>
    List<CrewModel>.from(json.decode(str).map((x) => CrewModel.fromJson(x)));

String crewModelToJson(List<CrewModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CrewModel {
  CrewModel({
    this.name,
    this.agency,
    this.image,
    this.wikipedia,
    this.launches,
    this.status,
    this.id,
  });

  String name;
  Agency agency;
  String image;
  String wikipedia;
  List<Launch> launches;
  Status status;
  String id;

  factory CrewModel.fromJson(Map<String, dynamic> json) => CrewModel(
        name: json["name"],
        agency: agencyValues.map[json["agency"]],
        image: json["image"],
        wikipedia: json["wikipedia"],
        launches:
            List<Launch>.from(json["launches"].map((x) => launchValues.map[x])),
        status: statusValues.map[json["status"]],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "agency": agencyValues.reverse[agency],
        "image": image,
        "wikipedia": wikipedia,
        "launches":
            List<dynamic>.from(launches.map((x) => launchValues.reverse[x])),
        "status": statusValues.reverse[status],
        "id": id,
      };
}

enum Agency { NASA, JAXA, ESA, SPACE_X }

final agencyValues = EnumValues({
  "ESA": Agency.ESA,
  "JAXA": Agency.JAXA,
  "NASA": Agency.NASA,
  "SpaceX": Agency.SPACE_X
});

enum Launch {
  THE_5_EB87_D46_FFD86_E000604_B388,
  THE_5_EB87_D4_DFFD86_E000604_B38_E,
  THE_5_FE3_AF58_B3467846_B324215_F
}

final launchValues = EnumValues({
  "5eb87d46ffd86e000604b388": Launch.THE_5_EB87_D46_FFD86_E000604_B388,
  "5eb87d4dffd86e000604b38e": Launch.THE_5_EB87_D4_DFFD86_E000604_B38_E,
  "5fe3af58b3467846b324215f": Launch.THE_5_FE3_AF58_B3467846_B324215_F
});

enum Status { ACTIVE }

final statusValues = EnumValues({"active": Status.ACTIVE});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
