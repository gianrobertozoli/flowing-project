import 'package:flowing_project/screens/crew/models/crew_model.dart';
import 'package:flowing_project/screens/crew/service/crew_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CrewController extends GetxController {
  var isLoading = true.obs;
  final crew = List<CrewModel>().obs;

  @override
  void onInit() {
    super.onInit();
    getCrew();
  }

  //Fetch Rockets
  void getCrew() {
    try {
      isLoading(true);
      CrewService.getCrew().then((resp) {
        isLoading(false);
        crew.value = resp;
      }, onError: (err) {
        isLoading(false);
        showSnackBar("Error", err.toString(), Colors.red);
      });
    } catch (exception) {
      isLoading(false);
      print('exc ${exception.toString()}');
      showSnackBar("Exception 1", exception.toString(), Colors.red);
    }
  }

  // snack bar
  showSnackBar(String title, String message, Color backgroundColor) {
    Get.snackbar(title, message,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: backgroundColor,
        colorText: Colors.white);
  }
}
