import 'package:flip_card/flip_card.dart';
import 'package:flowing_project/configuration/responsive_helper.dart';
import 'package:flowing_project/screens/crew/controllers/crew_controller.dart';
import 'package:flowing_project/screens/crew/models/crew_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class Crew extends StatelessWidget {
  CrewController controller = Get.put(CrewController());
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Obx(() {
      return controller.isLoading.value
          ? Container(
              child: Center(
                child: Text('Loading...'),
              ),
            )
          : ListView(
              shrinkWrap: true,
              children: [_buildArticlesGrid(mediaQuery)],
            );
    });
  }

  _buildArticlesGrid(MediaQueryData mediaQuery) {
    List<GridTile> tiles = [];
    controller.crew.forEach((crewMember) {
      tiles.add(_buildArticleTile(crewMember, mediaQuery));
    });
    return Padding(
      padding: responsivePadding(mediaQuery),
      child: GridView.count(
        crossAxisCount: responsiveNumGridTiles(mediaQuery),
        mainAxisSpacing: 30.0,
        crossAxisSpacing: 30.0,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: tiles,
      ),
    );
  }

  _buildArticleTile(CrewModel crewMember, MediaQueryData mediaQuery) {
    return GridTile(
        child: GestureDetector(
      onTap: () => {},
      child: FlipCard(
          direction: FlipDirection.HORIZONTAL,
          front: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: responsiveImageHeight(mediaQuery),
                  alignment: Alignment.bottomCenter,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    ),
                    image: DecorationImage(
                      image: NetworkImage(crewMember.image),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                alignment: Alignment.center,
                height: responsiveTitleHeight(mediaQuery),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0, 1),
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: Text(
                  crewMember.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                  maxLines: 2,
                ),
              ),
            ],
          ),
          back: Container(
            child: Column(
              children: [
                Text('Name: ${crewMember.name}'),
                Text('Name: ${crewMember.agency}'),
                Container(
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                      color: crewMember.status == 'active'
                          ? Colors.green
                          : Colors.grey,
                      shape: BoxShape.circle),
                ),
                InkWell(
                    onTap: () => _launchURL(crewMember.wikipedia),
                    child: Text('${crewMember.wikipedia}'))
              ],
            ),
          )),
    ));
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
