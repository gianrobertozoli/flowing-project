import 'package:flowing_project/configuration/urls.dart';
import 'package:flowing_project/screens/crew/models/crew_model.dart';
import 'package:flowing_project/services/network_manager.dart';

class CrewService {
  static NetworkManager manager = NetworkManager();

  static Future<List<CrewModel>> getCrew() async {
    try {
      final response = await manager.get(URLs.crew);
      if (response != null) {
        print("STATUS CODE: ${response.statusCode}");

        if (response.statusCode >= 200 && response.statusCode <= 299) {
          return crewModelFromJson(response.body);
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (exeption) {
      return Future.error(exeption.toString());
    }
  }
}
