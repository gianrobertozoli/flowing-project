import 'package:firebase_core/firebase_core.dart';
import 'package:flowing_project/app.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  await Firebase.initializeApp();
  runApp(App());
}
